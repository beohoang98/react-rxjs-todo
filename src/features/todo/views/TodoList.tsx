import {
  Checkbox,
  IconButton,
  LinearProgress,
  List,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText
} from "@mui/material";
import {FC, useState} from "react";
import {todoStore} from "../todo.store";
import {Todo, TodoStatus} from "../todo.type";
import dayjs from "dayjs";
import {DeleteRounded} from "@mui/icons-material";
import {useObservable} from "../../../core/utils/useObservable";
import {TodoListSkeleton} from "./TodoListSkeleton";

type TodoListProps = {
  data: Todo[];
}

export const TodoList: FC<TodoListProps> = ({data}) => {
  const [checking, setChecking] = useState(false);
  const [fetching] = useObservable(todoStore.loading$, false);

  const handleCheck = (todo: Todo) => () => {
    let nextStatus: TodoStatus = "done"
    if (todo.status === "done") {
      nextStatus = "open";
    }
    setChecking(true)
    todoStore
      .check(todo, nextStatus)
      .finally(() => setChecking(false));
  }

  const handleRemove = (todo: Todo) => () => {
    setChecking(true)
    todoStore
      .delete(todo.id)
      .finally(() => setChecking(false));
  }

  function formatTime(time?: Date) {
    if (!time) return null;
    return dayjs().to(time);
  }

  if (fetching) {
    return <TodoListSkeleton number={3} show />
  }

  return (
    <List>
      {(checking) && <LinearProgress variant="indeterminate"/>}
      {data.map((todo, idx) => (
        <ListItem key={todo.id || idx} divider>
          <ListItemIcon>
            <Checkbox disabled={checking} checked={todo.status === "done"} onChange={handleCheck(todo)}/>
          </ListItemIcon>
          <ListItemText primary={todo.name} secondary={formatTime(todo.time)}/>
          <ListItemSecondaryAction>
            <IconButton
              title="Delete"
              color="warning"
              size="small"
              onClick={handleRemove(todo)}
            >
              <DeleteRounded/>
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  );
}
