import {FC} from "react";
import {List, ListItem, ListItemIcon, ListItemText, Skeleton} from "@mui/material";

type TodoItemSkeletonProps = {
  number: number;
  show: boolean;
}

export const TodoListSkeleton: FC<TodoItemSkeletonProps> =
  ({
     number,
     show
   }) => {

    if (!show) return null;

    return (
      <List>
        {Array.from(Array(number)).map((_, idx) => (
          <ListItem key={idx}>
            <ListItemIcon>
              <Skeleton variant="circular" width={40} height={40} />
            </ListItemIcon>
            <ListItemText
              primary={<Skeleton variant="text" />}
              secondary={<Skeleton variant="text" />}
            />
          </ListItem>
        ))}
      </List>
    )
  }
