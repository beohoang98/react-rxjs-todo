import {ChangeEvent, FC} from "react";
import {Todo, TodoListQuery} from "../todo.type";
import {Button, Grid, MenuItem, TextField} from "@mui/material";
import {ArrowDownward, ArrowUpward, FilterList} from "@mui/icons-material";

type TodoFilterProps = {
  value: TodoListQuery;
  onChange: (val: TodoListQuery) => void;
}

export const TodoFilter: FC<TodoFilterProps> = ({value, onChange}) => {
  const { sort = "asc", sortBy = "id" } = value;

  const handleChangeSortBy = (ev: ChangeEvent<HTMLInputElement>) => {
    const key = ev.target.value as keyof Todo;
    onChange({
      ...value,
      sortBy: key,
    });
  }

  const handleChangeSort = () => {
    onChange({
      ...value,
      sort: sort === "asc" ? "desc" : "asc",
    });
  }

  return (
    <Grid container alignItems="flex-end" justifyContent="flex-end" padding="1em">
      <TextField
        select
        sx={{width: "100px"}}
        value={sortBy}
        variant="outlined"
        label="Sort By"
        onChange={handleChangeSortBy}
        margin="none"
        size="small"
      >
        <MenuItem value="" disabled>Sort By</MenuItem>
        <MenuItem value="id">ID</MenuItem>
        <MenuItem value="name">Name</MenuItem>
        <MenuItem value="time">Time</MenuItem>
      </TextField>
      <Button onClick={handleChangeSort} variant="text" color="secondary">
        {sort === "asc" ? <ArrowUpward/> : <ArrowDownward/>}
      </Button>
    </Grid>
  )
}
