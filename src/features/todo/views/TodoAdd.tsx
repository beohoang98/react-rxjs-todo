import {ChangeEvent, FC, FormEvent, useCallback, useState} from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Grid,
  IconButton,
  LinearProgress,
  SpeedDial,
  TextField
} from "@mui/material";
import {DateTimePicker} from "@mui/lab";
import {AddCircle} from "@mui/icons-material";
import {todoStore} from "../todo.store";
import {useObservable} from "../../../core/utils/useObservable";

export const TodoAdd: FC = () => {
  const [name, setName] = useState("");
  const [time, setTime] = useState<Date | null>(null);
  const [loading] = useObservable(todoStore.loading$, false);
  const [open, setOpen] = useState(false);

  const handleNameChange = (ev: ChangeEvent<HTMLInputElement>) => {
    setName(ev.target.value);
  }
  const handleTimeChange = (date: Date | null) => {
    setTime(date);
  }

  const handleSubmit = useCallback((ev: FormEvent) => {
    ev.preventDefault();
    if (loading || !open) return;
    if (!name) return;
    todoStore.add({
      id: "",
      name,
      time: time || new Date(),
      status: "open",
    }).then(() => {
      setName("");
      setTime(null)
      setOpen(false);
    })
      .catch(console.error);
  }, [loading, name, open, time]);

  return (
    <>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
      >
        <DialogTitle>Add Todo</DialogTitle>
        {loading && <LinearProgress variant="indeterminate"/>}
        <form action="#" onSubmit={handleSubmit}>
          <Grid container padding="1em" alignItems="center" gap="1em">
            <Grid item flex="1 1 auto">
              <TextField
                required
                label="Todo name"
                margin="dense"
                variant="outlined"
                name="todo-name"
                fullWidth
                value={name}
                onChange={handleNameChange}
                disabled={loading}
              />
            </Grid>
            <Grid item>
              <DateTimePicker
                label="Deadline"
                disabled={loading}
                onChange={handleTimeChange}
                value={time}
                renderInput={props => <TextField {...props} />}
              />
            </Grid>
            <DialogActions>
              <Button
                color="primary"
                variant="contained"
                type="submit"
                disabled={loading}
              >
                Add
              </Button>
            </DialogActions>
          </Grid>
        </form>
      </Dialog>
      <SpeedDial ariaLabel="add todo"
                 sx={{position: "fixed", bottom: "2em", right: "2em"}}
                 icon={<AddCircle/>}
                 title="add todo"
                 onClick={() => setOpen(true)}/>
    </>
  )
}
