import {Paper, Stack} from "@mui/material";
import {FC, useEffect, useState} from "react";
import {useObservable} from "../../../core/utils/useObservable";
import {todoStore} from "../todo.store";
import {TodoList} from "./TodoList";
import {TodoFilter} from "./TodoFilter";
import {TodoListQuery} from "../todo.type";
import {TodoAdd} from "./TodoAdd";

export const TodoPage: FC = () => {
  const [list] = useObservable(todoStore.list$, []);
  const [filter, setFilter] = useState<TodoListQuery>({});

  useEffect(() => {
    todoStore.list(filter);
  }, [filter]);

  return (
    <Stack>
      <TodoAdd />
      <Paper>
        <TodoFilter value={filter} onChange={setFilter} />
        <TodoList data={list}/>
      </Paper>
    </Stack>
  )
}
