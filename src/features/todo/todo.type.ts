export type TodoStatus = "open" | "done" | "in-progress" | "skipped";

export type Todo = {
    id: string;
    name: string;
    time?: Date;
    status?: TodoStatus;
}

export type TodoListQuery = {
    sort?: "asc" | "desc";
    sortBy?: keyof Todo;
    search?: string;
}
