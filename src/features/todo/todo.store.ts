import {BehaviorSubject} from "rxjs";
import {v4} from "uuid";
import {localStore} from "../../core/store";
import {LocalStore} from "../../core/store/local";
import {Todo, TodoListQuery, TodoStatus} from "./todo.type";

class TodoStore {
    constructor(
        private readonly store: LocalStore,
    ) { }

    private _list$ = new BehaviorSubject<Todo[]>([]);
    private _loading$ = new BehaviorSubject<boolean>(false);

    public list$ = this._list$.asObservable();
    public loading$ = this._loading$.asObservable();

    async add(todo: Todo) {
        this._loading$.next(true);
        todo.id = v4();
        try {
            await this.fakeWait(1000);
            this.store.doc("todo").set(todo.id, todo);
            const snapshot = this._list$.getValue();
            this._list$.next([...snapshot, todo]);
        } finally {
            this._loading$.next(false);
        }
    }

    async check(todo: Todo, status: TodoStatus) {
        const updatedTodo = {
            ...todo,
            status,
        };
        try {
            await this.fakeWait(1000);
            this.store.doc("todo").update(todo.id, {
                status,
            });
            const snapshot = this._list$.getValue();
            const oldTodoIdx = snapshot.findIndex(i => i.id === todo.id);
            if (oldTodoIdx < 0) return;
            snapshot[oldTodoIdx] = updatedTodo;
            this._list$.next([...snapshot]);
        } finally {

        }
    }

    get(id: string) {
        return this.store.doc("todo").get(id);
    }

    async list({
      search = "",
      sort = "asc",
      sortBy = "id"
    }: TodoListQuery) {
        this._loading$.next(true);
        try {
            await this.fakeWait(1000);
            const list = this.store.doc("todo")
              .list()
              .filter((todo: Todo) => todo.name.includes(search));
            list.sort(this.fastCompare(sortBy, sort));
            this._list$.next(list);
        } finally {
            this._loading$.next(false);
        }
    }

    async delete(id: string) {
        await this.fakeWait(1000);
        const newList = this._list$.getValue()
          .filter(todo => todo.id !== id);
        this.store.doc("todo").delete(id);
        this._list$.next(newList);
    }

    private fakeWait(ms: number) {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(null);
            }, ms)
        })
    }

    private fastCompare(field: string, direction: "asc" | "desc") {
        const factor = direction === "asc" ? 1 : -1;
        return (a: any, b: any) => {
            const valA = a[field];
            const valB = b[field];
            switch (true) {
                case typeof valA === "number": {
                    return (valA - valB) * factor;
                }
                case typeof valA === "string": {
                    return (valA as string).localeCompare(valB) * factor;
                }
                case valA instanceof Date: {
                    return ((valA as Date).getTime() - (valB as Date).getTime()) * factor;
                }
                default:
                    return 0;
            }
        }
    }
}

export const todoStore = new TodoStore(localStore);
