import {Subject} from "rxjs";
import {useObservable} from "./useObservable";
import {act, renderHook} from "@testing-library/react-hooks";

describe("useObservable", () => {
  test("Has default value", () => {
    const observable = new Subject<string[]>();
    // eslint-disable-next-line testing-library/render-result-naming-convention
    const hook = renderHook(() => useObservable(observable, ["YES"]));
    expect(hook.result.current[0]).toEqual(["YES"]);
    act(() => {
      observable.next(["NO"]);
    })
    expect(hook.result.current[0]).toEqual(["NO"]);
  })
});
