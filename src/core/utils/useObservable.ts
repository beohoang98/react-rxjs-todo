import { useEffect, useState } from "react";
import { Observable } from "rxjs";

export function useObservable<T = any>(ob: Observable<T>, defaultValue: T) {
    const [value, setValue] = useState<T>(defaultValue);
    useEffect(() => {
        const sub = ob.subscribe(setValue);
        return () => sub.unsubscribe();
    }, [ob]);

    return [value];
}
