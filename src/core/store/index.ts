import { LocalStore } from "./local";

export type StoreState = {
    todo: any;
}

export const localStore = new LocalStore<StoreState>();
