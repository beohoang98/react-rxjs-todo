export class LocalStore<S = any, K extends keyof S = keyof S> {
    private _docs: Record<K, LocalStoreEntity<S[K]>> = {} as any;

    get _store() {
        return window.localStorage;
    }

    doc(name: K): LocalStoreEntity<S[K]> {
        if (!this._docs[name]) {
            this._docs[name] = new LocalStoreEntity<any>(name as string, this._store);
        }
        return this._docs[name];
    }

    init() {
        for (let i = 0; i < this._store.length; ++i) {
            const key = this._store.key(i);
            if (!key) continue;
            // @ts-ignore
            this._docs[key] = new LocalStoreEntity<any>(key, this._store);
        }
    }
}

export class LocalStoreEntity<T extends any = any> {
    constructor(
        public readonly name: string,
        private readonly store: Storage,
    ) { }

    private _getRealKey(key: string) {
        return `${this.name}__${key}`;
    }

    set(key: string, value: T) {
        this.store.setItem(this._getRealKey(key), JSON.stringify(value));
    }

    get(key: string, deletedRecord = false): T | undefined {
        const strVal = (this.store.getItem(this._getRealKey(key)));
        if (strVal === "") {
            return strVal as any;
        }
        if (!strVal) return undefined;
        const data = JSON.parse(strVal);
        if (data && !data.deletedAt) {
            return data;
        }
        return undefined;
    }

    update(key: string, value: Partial<T>) {
        const exist = this.get(key);
        if (!exist) {
            this.set(key, value as T);
        } else {
            const merge = Object.assign({}, exist, value);
            this.set(key, merge);
        }
    }

    list() {
        const values: T[] = [];
        for (let i = 0; i < this.store.length; ++i) {
            const key = this.store.key(i);
            if (!key) continue;
            if (key.startsWith(`${this.name}__`)) {
                const val = this.get(key.replace(`${this.name}__`, ""));
                if (!val) continue;
                values.push(val)
            }
        }
        return values;
    }

    delete(key: string, safeDelete = true) {
        if (safeDelete) {
            return this.update(key, { deletedAt: new Date() } as any)
        }
        this.store.removeItem(this._getRealKey(key));
    }
}
