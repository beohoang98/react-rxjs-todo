import { AddCircleOutlined } from "@mui/icons-material";
import { AppBar, Container, IconButton, Toolbar, Typography } from "@mui/material";
import { TodoPage } from "./features/todo";
import {LocalizationProvider} from "@mui/lab";
import dayjsAdapter from "@mui/lab/AdapterDayjs";

function App() {
  return (
    <Provider>
      <AppBar position="sticky">
        <Container>
          <Toolbar disableGutters>
            <IconButton color="inherit" size="large" edge="start">
              <AddCircleOutlined fontSize="large" />
            </IconButton>
            <Typography variant="h4">Todo App</Typography>
          </Toolbar>
        </Container>
      </AppBar>
      <Container>
        <TodoPage />
      </Container>
    </Provider>
  );
}

function Provider({ children }: any) {
  return (
    <LocalizationProvider dateAdapter={dayjsAdapter}>
      {children}
    </LocalizationProvider>
  )
}

export default App;
