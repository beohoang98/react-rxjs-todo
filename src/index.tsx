import './index.css';
import { StrictMode } from 'react';
import { render } from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import * as sw from './registerServiceWorker';
import {localStore} from "./core/store";
import dayjs from "dayjs";
import dayjsDuration from "dayjs/plugin/duration";
import dayjsRelative from "dayjs/plugin/relativeTime";

async function bootstrap() {
  localStore.init();
  dayjs.extend(dayjsDuration);
  dayjs.extend(dayjsRelative);
  // fake wait
  await new Promise<void>((resolve) => {
    setTimeout(resolve, 1000);
  });
}

bootstrap()
  .then(() => {
    render(
      <StrictMode>
        <App/>
      </StrictMode>,
      document.getElementById('root')
    );
  });
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
sw.register();
